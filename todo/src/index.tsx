import * as ReactDOM from 'react-dom';
import { data } from './mock'
import { App } from './components/App/App'
import "./normalize.css";
import "./fonts.css";
import "./index.css";

ReactDOM.render(
    <App list={data} />,
    document.getElementById("root")
);

