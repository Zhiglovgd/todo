import * as React from "react";
import "./App.css";
import Input from "../Input/Input";
import TaskCard from "../TaskCard/TaskCard";
import Footer from "../Footer/Footer";
import { List, ListItem, View } from "../../types"

type Props = {
  list: List;
};

function convertStringToView(s: string): View {
  if (s === "All" || s === "Active" || s === "Completed") return s;
  else throw new Error("Wrong type View: " + s);
}

function filterTasks(type: View, arr: ListItem[]) {
  if (type === "All") return arr;
  if (type === "Active") return arr.filter((el) => !el.done);
  if (type === "Completed") return arr.filter((el) => el.done);
}

export const App: React.FC<Props> = ({ list }) => {
  const [tasks, setTasks] = React.useState(list);
  const [typeView, setTypeView] = React.useState<View>("All");
  const [textValue, setTextValue] = React.useState("");

  const handleChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTextValue(e.target.value);
  };

  const handleAddTask = () => {
    if (textValue) {
      const newTask: ListItem = {
        id: tasks.lastId + 1,
        text: textValue,
        done: false,
      };
      setTasks((prev) => {
        return {
          count: prev.count + 1,
          lastId: prev.lastId + 1,
          list: [...prev.list, newTask],
        };
      });
      setTextValue("");
    };
  }

  const handleAcceptTask = (id: number) => {
    const index: number = tasks.list.findIndex((el) => {
      return el.id === id;
    });
    setTasks((prev) => {
      return {
        count: prev.count,
        lastId: prev.lastId,
        list: [
          ...prev.list.slice(0, index),
          { ...prev.list[index], done: !prev.list[index].done },
          ...prev.list.slice(index + 1),
        ],
      };
    });
  };

  const handleClear = () => {
    const newList: ListItem[] = tasks.list.filter((el) => !el.done);
    setTasks((prev) => {
      return {
        count: newList.length,
        lastId: prev.lastId,
        list: [...newList],
      };
    });
  };

  const handleChangeType = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTypeView(convertStringToView(e.target.value));
  };

  const currentList = filterTasks(typeView, tasks.list)?.map((el) => (
    <li className="todo-list__item" key={el.id}>
      <TaskCard
        onClick={handleAcceptTask}
        value={el.done}
        text={el.text}
        id={el.id}
      />
    </li>
  ));

  const leftItems: number = tasks.list.reduce(
    (sum: number, item) => (item.done ? sum : sum + 1),
    0
  );

  return (
    <>
      <h1 className="todo-header">todos</h1>
      <div className="todo">
        <Input
          onClick={handleAddTask}
          value={textValue}
          onChange={handleChangeInput}
        />
        <ul className="todo-list">
          <>
            {currentList?.length ? (
              currentList
            ) : (
              <li className="todo-list__item todo-list__item_empty">Empty</li>
            )}
          </>
        </ul>
        <Footer
          onClickClr={handleClear}
          itemCount={leftItems}
          onClickType={handleChangeType}
          type={typeView}
        />
      </div>
    </>
  );
};
