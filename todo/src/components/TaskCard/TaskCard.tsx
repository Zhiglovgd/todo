import * as React from "react";
import "./TaskCard.css";

type Props = {
  onClick: (id: number) => void,
  value: boolean,
  text: string,
  id: number,
}

const TaskCard: React.FC<Props> = ({onClick, value, text, id}) => {
  return (
    <>
      <input
        className="todo-list__cbx-inp"
        id={`cbx-${id}`}
        type="checkbox"
        defaultChecked={value}
        onClick = {() => onClick(id)}
        style={{
        display: "none",
        }}
      />
      <label className="todo-list__cbx" htmlFor={`cbx-${id}`}>
        <span>
          <svg width="12px" height="9px" viewBox="0 0 12 9">
            <polyline points="1 5 4 8 11 1"></polyline>
          </svg>
        </span>
        <span>{text}</span>
      </label>
    </>
  );
};

export default TaskCard;
