import * as React from "react";
import "./Footer.css";
import { View } from "../../types"

type Props = {
  onClickClr: () => void,
  itemCount: number,
  onClickType: (e: React.ChangeEvent<HTMLInputElement>) => void,
  type: View,
}


const Footer: React.FC<Props> = ({ onClickClr, itemCount = 0, onClickType, type }) => {

  return (
    <div className="todo-footer">
      <div className="todo-counter">{itemCount} items left</div>
      <div className="todo-selector">
        <div className="radio">
          <input id="All" type="radio" onChange={(e) => onClickType(e)} name="type" value="All" checked={type === "All"} />
          <label htmlFor="All" className="noselect">{"All"}</label>

          <input id="Active" type="radio" onChange={(e) => onClickType(e)} name="type" value="Active" checked={type === "Active"} />
          <label htmlFor="Active" className="noselect">{"Active"}</label>

          <input id="Completed" type="radio" onChange={(e) => onClickType(e)} name="type" value="Completed" checked={type === "Completed"} />
          <label htmlFor="Completed" className="noselect">{"Completed"}</label>
        </div>
      </div>
      <button className="todo-clr-button noselect" onClick={() => onClickClr()}>{"Clear Completed"}</button>
    </div>
  )
}

export default Footer;