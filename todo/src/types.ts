export type ListItem = {
  id: number;
  done: boolean;
  text: string;
};

export type List = {
  count: number;
  lastId: number;
  list: Array<ListItem>;
};

export type View = "All" | "Active" | "Completed";