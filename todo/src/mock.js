export const data = {    
    count: 4,
    lastId: 3,
    list: [
        {
            id: 0,
            done: false,
            text: "Тестовое задание"  
        },
        {
            id: 1,
            done: true,
            text: "Прекрасный код"  
        },
        {
            id: 2,
            done: false,
            text: "Покрытие тестами"  
        },
        {
            id: 3,
            done: false,
            text: "Отправка задание"  
        },
        
    ]
}